﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class SpriteSwap : MonoBehaviour
{
    Image currentSprite;
    public Sprite pomodororSprite, breakSprite, iconPlay, iconPause, iconAudioEnabled, iconAudioDisabled;    
    
    // Sprites to swap must be assigned prior to start function in ProjectManager.cs
    void Awake()
    {
        currentSprite = this.GetComponent<Image>();
        if (pomodororSprite != null)
        {
            currentSprite.sprite = pomodororSprite;
        }   
        if (iconPlay != null)
        {
            currentSprite.sprite = iconPlay;
        }   
    }

    // Will call this method from ProjectManager.cs to swap sprites on all images to show
    // visual change between pomodorors and breaks.

    public void SwapSpriteToPomodoro()
    {
        // If statements used in the below mehtods will allow this script to be used on multiple GameObjects  
        // for swapping pomodoro & break, and play & pause.  Prents the need for an additional script as
        // you can only assign sprites to the GameObjects you wish to change with the below method calls.

        
        if(pomodororSprite != null)
        {
            currentSprite.sprite = pomodororSprite;
        }
    }

    public void SwapSpriteToBreak()
    {
        if (breakSprite != null)
        {
            currentSprite.sprite = breakSprite;
        }
    }

    public void SwapSpritePlay()
    {
        if(iconPlay != null)
        {
            currentSprite.sprite = iconPlay;
        }
    }

    public void SwapSpritePause()
    {
        if (iconPause != null)
        {
            currentSprite.sprite = iconPause;
        }
    }

    public void SwapSpriteAudio()
    {
        if ((iconAudioEnabled != null) && (currentSprite.sprite == iconAudioEnabled))
        {
            currentSprite.sprite = iconAudioDisabled;
        }
        else if ((iconAudioDisabled != null) && (currentSprite.sprite == iconAudioDisabled))
        {
            currentSprite.sprite = iconAudioEnabled;
        }
    }
}

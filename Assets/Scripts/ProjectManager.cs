﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

public class ProjectManager : MonoBehaviour
{
    // The goal of this console app is to create a simple Pomodoro Technique timer.
    // The Pomodoro Technique is a time management method that can be used for any task.
    // This technique is typically ussed to chopped up work into 20-25 minutes blocks of focused attention.
    // At the end of a block (aka. Pomodoro), a short break (typically 3-5 minutes) should allow you to step away from the task at hand.
    // Every 4 Pomodoros should allow for a longer break (15-30 minutes).

    // Timer will need to accept user input for the following: 
    // - durration Pomodoro blocks
    // - durration of short break (primary break)
    // - durration of long break (secondary break)
    // - number of Pomodoros between long breaks

    // Front-end goals:
    // - timer should be displayed as it is counting
    // - the ability to pause/start/restart the timer
    // - the ability to easily change the original user input
    // - the ability to change the way the app notifies the user (sound & volume)

    // **********************************************************************************************************************************

    // Audio plugin
    AudioManager audioManager;

    // UI Dropdown Menus
    [SerializeField]
    private TMP_Dropdown pomodoroTimerSelect;
    private TMP_Dropdown pomodoroRoundSelect;
    private TMP_Dropdown breakTimerPrimary;
    private TMP_Dropdown breakTimerSecondary;
    
    // UI Text Elements
    private TextMeshProUGUI textTimer;
    private TextMeshProUGUI textCurrentRound;
    private TextMeshProUGUI textCurretSettings;

    // UI Background - Visual Elements
    public Color colorBreak;
    public Color colorPomodoro;
    private Image uiTimerProgressBar;
    public GameObject uiSettings;
    private Button buttonOptions;
    private Button buttonStartStop;
    private Button buttonRestart;

    // List for sprite swap 
    [SerializeField]
    private List<GameObject> spritesToSwap = new List<GameObject>();

    // Creating a list of all the dropdowns
    List<TMP_Dropdown> listDropdowns = new List<TMP_Dropdown>();

    [Header("The max amount of minutes which will be populated in the dropdown")]
    public int pomodoroTimerMinutes = 60;
    public int pomodoroRounds = 10;
    public int breakPrimaryMinutes = 30;
    public int breakSecondaryMinutes = 60;

    // Used for logic in app.
    private bool timerIsRunning = false;
    private bool pomodoroRoundActive = true;
    private bool primaryBreak = true;
    private bool optionsMenuEnabled = false;
    private int currentRound;
    private int totalRounds;

    // Timers
    [SerializeField]
    private float timerCurrent;
    private float timerMax;
    public float objectMovementSpeed = 800f;

    // Lists for drop down menus
    List<int> listPomodoroTimerSelect = new List<int>();
    List<int> listPomodoroRoundSelect = new List<int>();
    List<int> listBreakTimerPrimary = new List<int>();
    List<int> listBreakTimerSecondary= new List<int>();

    private void Start()
    {
        audioManager = FindObjectOfType<AudioManager>();
        AssignmentOfUIFields();
        AssignmentOfTimerOptions();
        LoadPreferences();
        ButtonRestart();
    }

    private void Update()
    {
        if (timerIsRunning)
        {   
            timerCurrent -= Time.deltaTime;
            if (timerCurrent <= 0)
            {
                CallNextTimer();
            }
            UITimerUpdate();
        }
    }

    private void OnApplicationQuit()
    {
        SavePreferences();
    }

    private void AssignmentOfUIFields()
    {
        // Assignments of TMP Dropdowns * This could be cleaner to drag and drop each item in the Unity UI,
        // but I would like to do as much as possible via script.
        
        pomodoroTimerSelect = GameObject.Find("DropDown - PomodoroTimerSelect").GetComponent<TMP_Dropdown>();
        listDropdowns.Add(pomodoroTimerSelect);

        pomodoroRoundSelect = GameObject.Find("DropDown - PomodoroRoundSelect").GetComponent<TMP_Dropdown>();
        listDropdowns.Add(pomodoroRoundSelect);

        breakTimerPrimary = GameObject.Find("DropDown - BreakTimerPrimary").GetComponent<TMP_Dropdown>();
        listDropdowns.Add(breakTimerPrimary);

        breakTimerSecondary = GameObject.Find("DropDown - BreakTimerSecondary").GetComponent<TMP_Dropdown>();
        listDropdowns.Add(breakTimerSecondary);

        // Text UI Elements
        textTimer = GameObject.Find("Text - Timer").GetComponent<TextMeshProUGUI>();

        textCurrentRound = GameObject.Find("Text - CurrentRound").GetComponent<TextMeshProUGUI>();

        textCurretSettings = GameObject.Find("Text - CurrentSettings").GetComponent<TextMeshProUGUI>();

        uiTimerProgressBar = GameObject.Find("Image - TimerProgressBar").GetComponent<Image>();

        uiSettings = GameObject.Find("Folder - Settings");

        // Buttons
        buttonOptions = GameObject.Find("Button - Options").GetComponent<Button>();
        buttonStartStop = GameObject.Find("Button - Start/Stop").GetComponent<Button>();
        buttonRestart = GameObject.Find("Button - Restart").GetComponent<Button>();

        // Sprites to swap
        var tempList = FindObjectsOfType<SpriteSwap>();
        foreach (SpriteSwap objectContaingSpriteSwap in tempList)
        {
            spritesToSwap.Add(objectContaingSpriteSwap.transform.gameObject);
        }
    }

    private void AssignmentOfTimerOptions()
    {
        // Temp list for assigning values to dropdown.  This is a hacky fix as I 
        // can't seem to get values to read back from a list of MP_Dropdown.OptionData in
        // the needed format of int.  So I created a sperate list of int values for each dropdown.
        List<TMP_Dropdown.OptionData> listTemp = new List<TMP_Dropdown.OptionData>();

        // pomodoroTimer
        pomodoroTimerSelect.ClearOptions();
        for (int i = 5; i <= pomodoroTimerMinutes; i += 5)
        {
            string toAddToTimer = i.ToString();
            var timerOption = new TMP_Dropdown.OptionData(toAddToTimer + " - Minutes");
            listPomodoroTimerSelect.Add(i);
            listTemp.Add(timerOption);
        }
        pomodoroTimerSelect.AddOptions(listTemp);
        listTemp.Clear();

        // pomodoroRounds
        pomodoroRoundSelect.ClearOptions();
        
        for (int i = 1; i <= pomodoroRounds; i ++)
        {
            string toAddToTimer = i.ToString();
            var timerOption = new TMP_Dropdown.OptionData(toAddToTimer + " : 1 - Rounds");
            // Change wording on 'pomodoroRounds' to 'Rounds between Long Breaks.  
            // Changing list value, so if user selects 3 rounds, a long break will 
            // occur on every 4th round.
            listPomodoroRoundSelect.Add(i+1);
            listTemp.Add(timerOption);
        }
        pomodoroRoundSelect.AddOptions(listTemp);
        listTemp.Clear();

        // breakTimerPrimary
        breakTimerPrimary.ClearOptions();
        
        for (int i = 1; i <= breakPrimaryMinutes; i ++)
        {
            string toAddToTimer = i.ToString();
            var timerOption = new TMP_Dropdown.OptionData(toAddToTimer + " - Minutes");
            listBreakTimerPrimary.Add(i);
            listTemp.Add(timerOption);
        }
        breakTimerPrimary.AddOptions(listTemp);
        listTemp.Clear();

        // breakTimerSecondary
        breakTimerSecondary.ClearOptions();
        for (int i = 1; i <= breakSecondaryMinutes; i ++)
        {
            string toAddToTimer = i.ToString();
            var timerOption = new TMP_Dropdown.OptionData(toAddToTimer + " - Minutes");
            listBreakTimerSecondary.Add(i);
            listTemp.Add(timerOption);
        }
        breakTimerSecondary.AddOptions(listTemp);
        listTemp.Clear();
    }

    public void SavePreferences()
    {
        // Dropdown menus will now only save with button push.  Prevents the timer from
        // being edited while timer is running.  Must restart timer for changes to take 
        // effect.

        PlayerPrefs.SetInt("pomodoroTimerMinutes", pomodoroTimerSelect.value);
        PlayerPrefs.SetInt("pomodoroRounds", pomodoroRoundSelect.value);
        PlayerPrefs.SetInt("breakPrimaryMinutes", breakTimerPrimary.value);
        PlayerPrefs.SetInt("breakSecondaryMinutes", breakTimerSecondary.value);
        textCurretSettings.SetText("Current Settings");
        ButtonRestart();
    }

    private void LoadPreferences()
    {
        // Default positions if there are no PlayerPrefs saved.
        // This will be overwritten by a user when exiting the app.
        pomodoroTimerSelect.value = PlayerPrefs.GetInt("pomodoroTimerMinutes", 3);
        pomodoroRoundSelect.value = PlayerPrefs.GetInt("pomodoroRounds", 3);
        breakTimerPrimary.value = PlayerPrefs.GetInt("breakPrimaryMinutes", 4);
        breakTimerSecondary.value = PlayerPrefs.GetInt("breakSecondaryMinutes", 14);

        pomodoroTimerMinutes = listPomodoroTimerSelect[pomodoroTimerSelect.value];
        pomodoroRounds = listPomodoroRoundSelect[pomodoroRoundSelect.value];
        breakPrimaryMinutes = listBreakTimerPrimary[breakTimerPrimary.value];
        breakSecondaryMinutes = listBreakTimerSecondary[breakTimerSecondary.value];
        textCurretSettings.SetText("Current Settings");
    }

    public void ButtonOptions()
    {
        // Disables changing dropdowns when timer is running.
        if (optionsMenuEnabled == false)
        {
            optionsMenuEnabled = true;
            audioManager.Play("MenuOn");

            // Dropdown preferences will reset everytime the user access the settings menu
            // unless the save button is pushed.  This is so the settings will default to 
            // what is currently being used by the present timer.
            //LoadPreferences();

            // Disables starting/stopping or restarting the timer while configuring options
            buttonStartStop.interactable = false;
            buttonRestart.interactable = false;
            // Enables dropdown selection when timer is stopped
            foreach (TMP_Dropdown dropdown in listDropdowns)
            {
                dropdown.interactable = true;
            }
            StartCoroutine(MoveMenu(uiSettings, uiSettings.transform.position, new Vector3(0, 0, 0), objectMovementSpeed));
        }

        else
        {
            optionsMenuEnabled = false;
            audioManager.Play("MenuOff");
            buttonStartStop.interactable = true;
            buttonRestart.interactable = true;
            // Disables changing dropdowns when timer is running.
            foreach (TMP_Dropdown dropdown in listDropdowns)
            {
                dropdown.interactable = false;
            }
            StartCoroutine(MoveMenu(uiSettings, uiSettings.transform.position, new Vector3(0, 800, 0), objectMovementSpeed));
        }
    }

    public void ButtonStartStop()
    {
        // Starts and stops timer.
        // UI goal - change button text to "Stop" when running. 
        
        if (timerIsRunning == false)
        {   
            timerIsRunning = true;
            buttonOptions.interactable = false;
            buttonRestart.interactable = false;
            foreach (GameObject objectContaingSpriteSwap in spritesToSwap)
            {
                objectContaingSpriteSwap.GetComponent<SpriteSwap>().SwapSpritePause();
            }
            audioManager.Play("Play");
        }

        else
        {
            timerIsRunning = false;
            buttonOptions.interactable = true;
            buttonRestart.interactable = true;
            foreach (GameObject objectContaingSpriteSwap in spritesToSwap)
            {
                objectContaingSpriteSwap.GetComponent<SpriteSwap>().SwapSpritePlay();
            }
            audioManager.Play("Pause");
        }
    }

    public void ButtonRestart()
    {
        // Restarts timer *Will also stop timer if pushed and timer is running
        if (timerIsRunning == true)
        {
            ButtonStartStop();
        }
        pomodoroRoundActive = true;
        LoadPreferences();
        timerMax = pomodoroTimerMinutes * 60;
        timerCurrent = timerMax;
        currentRound = 1;
        totalRounds = 1;
        UITimerUpdate();
        UIRoundBreakUpdate();
    }

    public void ButtonAudio()
    {
        foreach (GameObject objectContaingSpriteSwap in spritesToSwap)
        {
            objectContaingSpriteSwap.GetComponent<SpriteSwap>().SwapSpriteAudio();
        }

        // Enable Disable Audio
        if (audioManager.gameObject.activeInHierarchy == true)
        {
            audioManager.gameObject.SetActive(false);
        }   
        else
        {
            audioManager.gameObject.SetActive(true);
        }
    }

    public void UniversalButtonPush()
    {
        // For playing a sound on any given button
        audioManager.Play("UniversalButtonPush");
    }

    public void UniversalDropdownSelect()
    {
        // For playing a sound on any given dropdown
        audioManager.Play("DropDownValueChanged");
    }



    public void DropDownValueChanged()
    {
        // Reconfigures timer settings when adjusted by user.
        // Currently doesn't do anything.  Moved functionality to 

        // Maybe create a text UI field that will generate the message:
        // "You must save for changes to take effect"
        audioManager.Play("UniversalButtonPush");
        textCurretSettings.SetText("*Unsaved Settings");
    }

    private void CallNextTimer()
    {
        totalRounds++;

        if (totalRounds % 2 == 0)
        {
            CallBreakTimer();
        }

        else
        {
            CallPomodoroTimer();
        }

        UIRoundBreakUpdate();
    }

    private void CallPomodoroTimer()
    {
        pomodoroRoundActive = true;

        // Plug-in for break sound and background change.
        
        timerMax = pomodoroTimerMinutes * 60;
        timerCurrent = timerMax;
        currentRound++;
    }

    private void CallBreakTimer()
    {
        pomodoroRoundActive = false;

        // Plug-in for break sound and background change.

        if (currentRound % pomodoroRounds == 0)
        {
            timerMax = breakSecondaryMinutes * 60;
            timerCurrent = timerMax;
            primaryBreak = false;
        }

        else
        {
            timerMax = breakPrimaryMinutes * 60;
            timerCurrent = timerMax;
            primaryBreak = true;
        }
    }

    private void UITimerUpdate()
    {
        string minutes = ((int)timerCurrent / 60).ToString();
        string seconds = (timerCurrent % 60).ToString("f1");
        textTimer.SetText(minutes + ":" + seconds);
        uiTimerProgressBar.fillAmount = timerCurrent / timerMax;
    }

    private void UIRoundBreakUpdate()
    {   
        // Pomodoro Round
                
        if (pomodoroRoundActive == true)
        {
            if(timerIsRunning == true)
            {
                audioManager.Play("TimerChangePomodoro");
            }

            // Swaps the sprites of all buttons
            foreach (GameObject objectContaingSpriteSwap in spritesToSwap)
            {
                objectContaingSpriteSwap.GetComponent<SpriteSwap>().SwapSpriteToPomodoro();
            }
            textCurrentRound.SetText("Focus Round: " + currentRound);
            // UI color update here
            uiTimerProgressBar.color = colorPomodoro;
        }

        // Break Round

        else
        {
            if(timerIsRunning == true)
            {
                audioManager.Play("TimerChangeBreak");
            }

            // Swaps the sprites of all buttons
            foreach (GameObject objectContaingSpriteSwap in spritesToSwap)
            {
                objectContaingSpriteSwap.GetComponent<SpriteSwap>().SwapSpriteToBreak();
            }

            if (primaryBreak == true)
            {
                textCurrentRound.SetText("Primary Break: " + currentRound);
            }
            else
            {
                textCurrentRound.SetText("Secondary Break: " + currentRound);
            }
            // UI color update here
            uiTimerProgressBar.color = colorBreak;
        }
    }

    // For moving menu items in and out of view of the camera
    // Pulled from Unity docs.  Requires clean-up.    
    IEnumerator MoveMenu(GameObject objectToMove, Vector3 oldPosition, Vector3 newPosition, float objectMovementSpeed)
    {
        // Prevents the buttonOptions from being pushed again while this is running.
        buttonOptions.interactable = false;

        float coroutineRunningTime = 0;
        float startTime;
        // Total distance between the oldPosition and newPosition.
        float journeyLength;

        // Keep a note of the time the movement started.
        startTime = Time.time;
        // Calculate the journey length.
        journeyLength = Mathf.Abs(Vector3.Distance(oldPosition, newPosition));

        // Follows the target position like with a spring
        while (objectToMove.transform.position != newPosition)
        {
            coroutineRunningTime += Time.deltaTime;
            // Distance moved = time * objectMovementSpeed.
            float distCovered = (Time.time - startTime) * objectMovementSpeed;

            // Fraction of journey completed = current distance divided by total distance.
            float fracJourney = distCovered / journeyLength;

            // Set our position as a fraction of the distance between the markers.
            objectToMove.transform.position = Vector3.Lerp(oldPosition, newPosition, fracJourney);
            yield return null;
        }
        objectToMove.transform.position = newPosition;
        buttonOptions.interactable = true;
        StopCoroutine(MoveMenu(objectToMove, oldPosition, newPosition, objectMovementSpeed));
    }
}
